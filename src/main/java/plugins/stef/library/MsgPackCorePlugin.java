package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the MasgPack Core library.
 * 
 * @author Stephane Dallongeville
 */
public class MsgPackCorePlugin extends Plugin implements PluginLibrary
{
    //
}
